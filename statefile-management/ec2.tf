resource "aws_instance" "webserver" {
  ami           = "ami-08a0d1e16fc3f61ea"
  instance_type = "t3.nano"
  tags = {
    Name = "${terraform.workspace}-instance"
  }
}


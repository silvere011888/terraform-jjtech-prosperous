
terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket = "bucket-statefile0219"
    key    = "prosperous/terraform.tfstate"
    region = "us-east-1"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "dynamoDB-statefile"
  }
}
module "web_server_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"

  name        = "prosperous-sg"
  description = "Security group for web-server with HTTP ports open within VPC"
vpc_id      = "vpc-06c088616adc9861b"

  ingress_cidr_blocks = ["10.10.0.0/16"]
}
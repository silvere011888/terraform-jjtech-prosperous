terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.45.0"
    }
  }
}

#provider "aws" {
#profile = "default"
# region  = "us-east-2"
#}

provider "aws" {
  shared_config_files      = ["/Users/silvere Tiendrebeogo/.aws/config"]
  shared_credentials_files = ["/Users/silvere Tiendrebeogo/.aws/credentials"]
  profile                  = "default"
}
module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"

  name = "prosperous-EC2"

  instance_type          = "t2.micro"

  monitoring             = true
  vpc_security_group_ids = ["sg-054fb335f0f9a915e"]
  

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
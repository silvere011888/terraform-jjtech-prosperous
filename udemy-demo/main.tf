resource "aws_security_group" "udemy_demo" {
    name        = "udemy_demo"
    description = "Allow TLS inbound traffic"
  
}

resource "aws_security_group_rule" "Allow_ssh" {
  type="ingress"
    security_group_id =aws_security_group.udemy_demo.id
  from_port = 22
  to_port = 22
  protocol = "ssh"
  cidr_blocks =var.cidr_blocks
}
 resource "aws_security_group_rule" "allow-all-outbound" {
  type="egress"
    security_group_id =aws_security_group.udemy_demo.id
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks =var.cidr_blocks 
   
 }
# data "aws_ami" "linux2" {

#   most_recent = true

#   owners      = ["amazon"]

#   filter {

#     name   = "virtualization-type"

#     values = ["hvm"]

#   }

#   filter {

#     name   = "name"

#     values = ["Windows_Server-2022-English-Full-Base-*"] # Windows_Server-2022-English-Full-Base-2023.04.12

#   }

 

# }



module "ec2" {
    source = "../child-module/ec2"
    Do-you-want-to-create-ec2-instance = true
    ami_bliss_id =  "ami-08a0d1e16fc3f61ea"   #data.aws_ami.linux2.id # Get the AMI   data.
    instance_type = "t2.nano"
    key_name= "terraform-key"
    subnet_id = module.vpc.public_subnet_id
tags ={
    Name = "webserver "
    Environment = "dev "
    created_by = "terraform "
} 

    sg_ids = [module.sg.sg_id]
  
}

module "vpc" {
    source = "../child-module/vpc"
    cidr = "10.0.0.0/16"
    vpc_tags = {
        Name = "my-vpc"
        env = "dev"
    }
    subnet_cidr = "10.0.0.0/24"
}

module "sg" {
    source = "../child-module/security-group"
   aws_security_group = "sg-webserver" 
   

    
    vpc_id = module.vpc.vpc_id

    # ssh_port = 22
  
}
variable "region" {
    type = string
  default = "us-east-1"
  description = "AWS region"
}
variable "profile" {
type = string
  default = "default"
  description = "AWS profile"
}
variable "cidr" {


  type = string


  description = "provide cidr block for vpc"


  default = "10.0.0.0/16"
  }



 


variable "vpc_tags" {
    type = map(string)
  description = "vpc tags"
  default = {
    Name = "my-vpc"
    env = "dev"
  }
}

variable "subnet_cidr" {
    type = string
    default = "10.0.0.0/24"
    description = "value of cidr block"
}


  
